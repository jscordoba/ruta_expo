-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-11-2016 a las 06:47:13
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ruta_expo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aduana`
--

CREATE TABLE IF NOT EXISTS `aduana` (
  `id` int(11) NOT NULL,
  `gasto_dolar` int(11) NOT NULL,
  `update_last` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aduana`
--

INSERT INTO `aduana` (`id`, `gasto_dolar`, `update_last`) VALUES
(1, 140, '2016-11-08 12:01:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenedor`
--

CREATE TABLE IF NOT EXISTS `contenedor` (
  `id` int(20) NOT NULL,
  `unidad` varchar(20) NOT NULL,
  `cantidad` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contenedor`
--

INSERT INTO `contenedor` (`id`, `unidad`, `cantidad`) VALUES
(1, 'pies', 20),
(2, 'pies', 40);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cont_vs_peso`
--

CREATE TABLE IF NOT EXISTS `cont_vs_peso` (
  `id` int(11) NOT NULL,
  `id_puerto_nal` int(11) NOT NULL,
  `id_contenedor` int(11) NOT NULL,
  `id_peso_tara` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cont_vs_peso`
--

INSERT INTO `cont_vs_peso` (`id`, `id_puerto_nal`, `id_contenedor`, `id_peso_tara`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 3),
(3, 1, 1, 5),
(4, 1, 1, 6),
(5, 1, 1, 10),
(6, 1, 2, 2),
(7, 1, 2, 4),
(8, 1, 2, 6),
(9, 1, 2, 11),
(12, 2, 1, 1),
(13, 2, 1, 3),
(14, 2, 1, 5),
(15, 2, 1, 6),
(16, 2, 1, 10),
(17, 2, 2, 2),
(18, 2, 2, 4),
(19, 2, 2, 6),
(20, 2, 2, 9),
(21, 2, 2, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peso_tara`
--

CREATE TABLE IF NOT EXISTS `peso_tara` (
  `id` int(11) NOT NULL,
  `peso_tara` varchar(20) NOT NULL,
  `unidad` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `peso_tara`
--

INSERT INTO `peso_tara` (`id`, `peso_tara`, `unidad`) VALUES
(2, '11', 'Ton.'),
(3, '12', 'Ton.'),
(4, '14', 'Ton.'),
(5, '16', 'Ton.'),
(6, '20', 'Ton.'),
(9, '22', 'Ton.'),
(10, '23', 'Ton.'),
(11, '26', 'Ton.'),
(1, '6', 'Ton.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerto`
--

CREATE TABLE IF NOT EXISTS `puerto` (
  `id` int(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `ciudad` varchar(30) NOT NULL,
  `pais` varchar(30) NOT NULL,
  `Tipo` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `puerto`
--

INSERT INTO `puerto` (`id`, `nombre`, `ciudad`, `pais`, `Tipo`) VALUES
(1, 'Puerto de Buenaventura', 'Buenaventura', 'Colombia', 'Nacional'),
(2, 'Puerto de Cartagena', 'Cartagena', 'Colombia', 'Nacional'),
(3, 'Puerto de Oakland', 'Oakland', 'United States', 'Internacional'),
(4, 'Puerto de New York', 'New York', 'United States', 'Internacional'),
(5, 'Puerto de Norfolk', 'Norfolk', 'United States', 'Internacional'),
(8, 'Puerto de Miami', 'Miami', 'United States', 'Internacional'),
(9, 'Puerto de Los Angeles', 'Los Angeles', 'United States', 'Internacional'),
(10, 'Puero de Long Beach', 'Long Beach', 'United States', 'Internacional'),
(11, 'Puerto de Houston', 'Houston', 'United States', 'Internacional'),
(12, 'Puerto de Mobile', 'Mobile', 'United States', 'Internacional'),
(13, 'Puerto de Savannah', 'Savannah', 'United States', 'Internacional'),
(14, 'Puerto de Charleston', 'Charleston', 'United States', 'Internacional'),
(15, 'Puerto de New Orleans', 'New Orleans', 'United States', 'Internacional'),
(16, 'Puerto de Seatle', 'Seatle', 'United States', 'Internacional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerto_internacional`
--

CREATE TABLE IF NOT EXISTS `puerto_internacional` (
  `id` int(20) NOT NULL,
  `puerto_id_inter` int(20) NOT NULL,
  `puerto_id_nal` int(20) NOT NULL,
  `contenedor_id` int(20) NOT NULL,
  `flete_inter_dolar` varchar(30) NOT NULL,
  `estimado_dias` int(4) NOT NULL,
  `frecuencia` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `puerto_internacional`
--

INSERT INTO `puerto_internacional` (`id`, `puerto_id_inter`, `puerto_id_nal`, `contenedor_id`, `flete_inter_dolar`, `estimado_dias`, `frecuencia`) VALUES
(1, 3, 1, 1, '1200', 15, 'Semanal'),
(2, 3, 1, 2, '1600', 15, 'Semanal'),
(4, 4, 1, 1, '1100', 23, 'Semanal'),
(5, 4, 2, 1, '800', 16, 'Semanal'),
(6, 4, 1, 2, '1300', 23, 'Semanal'),
(7, 4, 2, 2, '1050', 16, 'Semanal'),
(8, 8, 1, 1, '1250', 16, 'Semanal'),
(9, 9, 1, 1, '870', 15, 'Semanal'),
(10, 10, 1, 1, '1300', 18, 'Semanal'),
(11, 5, 2, 1, '750', 18, 'Semanal'),
(12, 8, 2, 1, '1050', 5, 'Semanal'),
(13, 12, 2, 1, '1050', 19, 'Semanal'),
(14, 13, 2, 1, '700', 17, 'Semanal'),
(15, 14, 2, 1, '700', 18, 'Semanal'),
(16, 15, 2, 1, '850', 11, 'Semanal'),
(18, 5, 2, 2, '950', 18, 'Semanal'),
(19, 8, 2, 2, '1550', 5, 'Semanal'),
(20, 11, 1, 1, '1100', 20, 'Semanal'),
(21, 8, 1, 2, '1350', 16, 'Semanal'),
(22, 11, 1, 2, '1250', 20, 'Semanal'),
(23, 9, 1, 2, '1050', 15, 'Semanal'),
(24, 10, 1, 2, '1600', 18, 'Semanal'),
(28, 16, 2, 1, '1000', 18, 'Semanal'),
(29, 12, 2, 2, '1350', 19, 'Semanal'),
(30, 13, 2, 2, '950', 17, 'Semanal'),
(31, 14, 2, 2, '950', 18, 'Semanal'),
(32, 15, 2, 2, '950', 11, 'Semanal'),
(33, 16, 2, 2, '1220', 18, 'Semanal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerto_nacional`
--

CREATE TABLE IF NOT EXISTS `puerto_nacional` (
  `id` int(20) NOT NULL,
  `id_puerto` int(20) NOT NULL,
  `id_contenedor` int(20) NOT NULL,
  `id_peso_tara` int(11) NOT NULL,
  `flete_terrestre_dolar` varchar(30) NOT NULL,
  `gasto_portuario_dolar` varchar(30) NOT NULL,
  `id_gasto_aduana` int(20) NOT NULL DEFAULT '1',
  `id_trm` int(20) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `puerto_nacional`
--

INSERT INTO `puerto_nacional` (`id`, `id_puerto`, `id_contenedor`, `id_peso_tara`, `flete_terrestre_dolar`, `gasto_portuario_dolar`, `id_gasto_aduana`, `id_trm`) VALUES
(1, 2, 1, 1, '831', '450', 1, 1),
(3, 1, 1, 1, '459.67', '350', 1, 1),
(5, 1, 1, 3, '517.67', '350', 1, 1),
(6, 1, 1, 5, '542.67', '350', 1, 1),
(7, 1, 1, 6, '595.33', '350', 1, 1),
(8, 1, 1, 10, '581.33', '350', 1, 1),
(9, 1, 2, 2, '504', '450', 1, 1),
(10, 1, 2, 4, '504', '450', 1, 1),
(11, 1, 2, 6, '576', '450', 1, 1),
(12, 1, 2, 11, '581.33', '450', 1, 1),
(13, 2, 1, 3, '991.13', '450', 1, 1),
(14, 2, 1, 5, '1041', '450', 1, 1),
(15, 2, 1, 6, '1174.33', '450', 1, 1),
(16, 2, 1, 10, '1174.33', '450', 1, 1),
(17, 2, 2, 2, '930.33', '550', 1, 1),
(18, 2, 2, 4, '1107.67', '550', 1, 1),
(19, 2, 2, 6, '1174.33', '550', 1, 1),
(20, 2, 2, 9, '1174.33', '550', 1, 1),
(21, 2, 2, 11, '1201.67', '550', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `p_inter_vs_nal`
--

CREATE TABLE IF NOT EXISTS `p_inter_vs_nal` (
  `id` int(20) NOT NULL,
  `id_puerto_inter` int(20) NOT NULL,
  `id_puerto_nal` int(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `p_inter_vs_nal`
--

INSERT INTO `p_inter_vs_nal` (`id`, `id_puerto_inter`, `id_puerto_nal`) VALUES
(1, 3, 1),
(3, 4, 1),
(4, 4, 2),
(9, 5, 2),
(5, 8, 1),
(10, 8, 2),
(6, 9, 1),
(7, 10, 1),
(8, 11, 1),
(14, 12, 2),
(15, 13, 2),
(16, 14, 2),
(17, 15, 2),
(18, 16, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trm_dolar`
--

CREATE TABLE IF NOT EXISTS `trm_dolar` (
  `id` int(20) NOT NULL,
  `valor_pesos` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `trm_dolar`
--

INSERT INTO `trm_dolar` (`id`, `valor_pesos`) VALUES
(1, '3000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(25) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `usuario`, `clave`) VALUES
(1, 'Andres Hoyos', 'jcomex', '123');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aduana`
--
ALTER TABLE `aduana`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contenedor`
--
ALTER TABLE `contenedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cont_vs_peso`
--
ALTER TABLE `cont_vs_peso`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_puerto_nal` (`id_puerto_nal`,`id_contenedor`,`id_peso_tara`);

--
-- Indices de la tabla `peso_tara`
--
ALTER TABLE `peso_tara`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `peso_tara` (`peso_tara`,`unidad`);

--
-- Indices de la tabla `puerto`
--
ALTER TABLE `puerto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `puerto_internacional`
--
ALTER TABLE `puerto_internacional`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `puerto_id_inter` (`puerto_id_inter`,`puerto_id_nal`,`contenedor_id`);

--
-- Indices de la tabla `puerto_nacional`
--
ALTER TABLE `puerto_nacional`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_puerto` (`id_puerto`,`id_contenedor`,`id_peso_tara`);

--
-- Indices de la tabla `p_inter_vs_nal`
--
ALTER TABLE `p_inter_vs_nal`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_puerto_inter` (`id_puerto_inter`,`id_puerto_nal`);

--
-- Indices de la tabla `trm_dolar`
--
ALTER TABLE `trm_dolar`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aduana`
--
ALTER TABLE `aduana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `contenedor`
--
ALTER TABLE `contenedor`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `cont_vs_peso`
--
ALTER TABLE `cont_vs_peso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `peso_tara`
--
ALTER TABLE `peso_tara`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `puerto`
--
ALTER TABLE `puerto`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `puerto_internacional`
--
ALTER TABLE `puerto_internacional`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `puerto_nacional`
--
ALTER TABLE `puerto_nacional`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `p_inter_vs_nal`
--
ALTER TABLE `p_inter_vs_nal`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `trm_dolar`
--
ALTER TABLE `trm_dolar`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
