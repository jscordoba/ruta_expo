<div class="row">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">
	        <!-- <img alt="Brand" src="..."> -->
	      </a>
	      <span class="navbar-text navbar-left"> <h3><b>Consulta de Rutas de Exportación </b> <small> Magnetron S.A.S</small></h3></span>
	    </div>
	  </div>
	</nav>
</div>

<div class="container">
	<div class="img_map">
	</div>
</div>

<div class="row">
	<div class="col-md-6">
	
		<form class="form-horizontal">
			<fieldset>

			<!-- Form Name -->
			<legend>Rutas Nacionales</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="salida">Salida</label>  
			  <div class="col-md-4">
			  <input id="salida" name="salida" type="text" placeholder="La Tebaida" value="La Tebaida" class="form-control input-md" readonly>
			  <span class="help-block">Salida de Contenedor</span>  
			  </div>
			</div>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="contenedor">Contenedor *</label>
			  <div class="col-md-4">
			    <select id="contenedor" name="contenedor" class="form-control" onchange="limpiar(this.value);" required>
			      <option value="">Seleccione Una opción</option>
			      <?php foreach ($contenedor->result() as $conten): ?>
			      	<option value="<?= $conten->id ?>"><?=  $conten->cantidad." ".$conten->unidad ?></option>
			      <?php endforeach ?>
			    </select>
			  </div>
			</div>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="puerto_origen">Puerto Origen *</label>
			  <div class="col-md-4">
			    <select id="puerto_origen" name="puerto_origen" class="form-control" onchange="llenarPeso();"  required>
			      <option value="">Seleccione una opción</option>
			       
			      <?php foreach ($puerto_origen->result() as $puerto): ?>
			      	<option value="<?= $puerto->id ?>"><?=  $puerto->nombre ?></option>
			      <?php endforeach ?>
			      
			    </select>
			  </div>
			</div>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="peso_tara">Peso *</label>
			  <div class="col-md-4">
			    <span id="select_peso_tara">
			    	
			    </span>
			  </div>
			</div>


			</fieldset>
		</form>

		<br>

		<form class="form-horizontal">
			<fieldset>

			<!-- Form Name -->
			<legend>Rutas Internacionales</legend>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="puerto_destino">Puerto</label>
			  <div class="col-md-4">
			    <span id="select_puerto_inter">
			    	
			    </span>
			  </div>
			</div>

			</fieldset>
		</form>

	</div>


	<div class="col-md-6">
		<form class="form-horizontal">
			<fieldset>

			<!-- Form Name -->
			<legend>Valores FOB</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="valor_dolar_FOB">Valor en Dolares</label>  
			  <div class="col-md-4">
			  <input id="valor_dolar_FOB" name="valor_dolar_FOB" type="text" placeholder="3.000 USD" class="form-control input-md" readonly>
			  <span class="help-block">Valor en USD</span>  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="valor_pesos_FOB">Valor en Pesos</label>  
			  <div class="col-md-4">
			  <input id="valor_pesos_FOB" name="valor_pesos_FOB" type="text" placeholder="$1.000.000" class="form-control input-md" readonly>
			  <span class="help-block">Valor en $ COP</span>  
			  </div>
			</div>

			</fieldset>
		</form>
		
		<br><br>


		<form class="form-horizontal">
			<fieldset>

			<!-- Form Name -->
			<legend>Valor Total CIF</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="valor_dolar_CIF">Valor en Dolares</label>  
			  <div class="col-md-4">
			  <input id="valor_dolar_CIF" name="valor_dolar_CIF" type="text" placeholder="3.000 USD" class="form-control input-md" readonly>
			  <span class="help-block">Valor en USD</span>  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="valor_pesos_CIF">Valor en Pesos</label>  
			  <div class="col-md-4">
			  <input id="valor_pesos_CIF" name="valor_pesos_CIF" type="text" placeholder="$ 1.000.000" class="form-control input-md" readonly>
			  <span class="help-block">Valor en $ COP</span>  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="campo_diasTransito">Días de Tránsito</label>  
			  <div class="col-md-4">
			  <input id="campo_diasTransito" name="campo_diasTransito" type="text" placeholder="#" class="form-control input-md" readonly>
			  <span class="help-block">Campo reflejado en días</span>  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="campo_frecuencia">Frecuencia</label>  
			  <div class="col-md-4">
			  <input id="campo_frecuencia" name="campo_frecuencia" type="text" placeholder="#" class="form-control input-md" readonly>
			  <span class="help-block">... </span>  
			  </div>
			</div>

			</fieldset>
		</form>

	</div>
	
</div>