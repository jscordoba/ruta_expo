<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

			</div>
		</div>
	</div>

    <!-- jQuery -->
    <script src="styles/template/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="styles/template/js/bootstrap.min.js"></script>
    <!-- Morris Charts JavaScript -->
    <script src="styles/template/js/plugins/morris/raphael.min.js"></script>
    <script src="styles/template/js/plugins/morris/morris.min.js"></script>
    <script src="styles/template/js/plugins/morris/morris-data.js"></script>

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

    <script src="styles/template/js/custom_style.js"></script>
    
    
    
</body>
</html>