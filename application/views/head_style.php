<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ruta Expo - Magnetron S.A.S</title>

    <!-- Bootstrap Core CSS -->
    <link href="styles/template/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style CSS JSCORDOBA -->
    <link href="styles/template/css/style_css_custom.css" rel="stylesheet">
    <!-- Custom CSS -->
    <!-- <link href="styles/template/css/sb-admin.css" rel="stylesheet"> -->
    <!-- Custom Fonts -->
    <link href="styles/template/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <script src="styles/template/js/jquery.js"></script>
    
    
    <script>
        $(document).ready(function() {
            $('.table.display').DataTable();
            //alert("OK");
        } );
    </script>

</head>
<body>

    <div id="wrapper">

        <div id="page-wrapper">

            <div class="container-fluid">

            
