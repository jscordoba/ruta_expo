<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$this->load->view('head_style');

		$data['contenedor'] = $this->db->get('contenedor');
		$data['peso_tara'] = $this->db->get('peso_tara');

		$this->db->where('tipo', 'Nacional');
		$data['puerto_origen'] = $this->db->get('puerto');

		$this->load->view('consulta_ruta',$data);
		$this->load->view('footer_style');
		
	}

	public function verPuertosInter()
	{
		# code...
	}

	public function verPuertosNal()
	{
		# code...
	}

	public function verContenedores()
	{
		# code...
	}

	public function llenarPeso()
	{
		# code...
		$id_contenedor = $this->input->post('id_contenedor');
		$id_puerto_nal = $this->input->post('id_puerto_origen');

		try {
			$this->db->join('cont_vs_peso', 'cont_vs_peso.id_peso_tara = peso_tara.id', 'join');
			$this->db->where('cont_vs_peso.id_puerto_nal', $id_puerto_nal);
			$this->db->where('cont_vs_peso.id_contenedor', $id_contenedor);
			$this->db->select('peso_tara.id,peso_tara.peso_tara,peso_tara.unidad');
			$resul = $this->db->get('peso_tara');
			
		} catch (Exception $e) {
			echo "Error en la ejecución de la consulta: ".$e;
		}

		if ($resul->num_rows()>0) {
			# code...
			?>

			<select id="peso_tara" name="peso_tara" class="form-control" onchange="calcularFOB(this.value);"  required><!--  -->
				<option value="">Seleccione una Opción</option>	
			
			<?php foreach ($resul->result() as $peso_tara): ?>
				<option value="<?=  $peso_tara->id ?>"><?= $peso_tara->peso_tara." ".$peso_tara->unidad ?> + TARA</option>
			<?php endforeach ?>
			
			</select>

			<?php
		}else{
			echo "Sin Datos.";
		}

	}

	public function llenarPuertosInter()
	{
		# code...
		$puerto_id_nal = $this->input->post('puerto_id_nal');
		$valor_pesos_FOB = $this->input->post('valor_pesos_FOB');
		$valor_dolar_FOB = $this->input->post('valor_dolar_FOB');

		try {
			$this->db->join('puerto', 'puerto.id = p_inter_vs_nal.id_puerto_inter', 'join');
			$this->db->where('id_puerto_nal', $puerto_id_nal);
			$resul = $this->db->get('p_inter_vs_nal');
			
		} catch (Exception $e) {
			echo "Error en la ejecución de la consulta: ".$e;
		}

		if ($resul->num_rows()>0) {
			# code...
			?><select id="puerto_destino" name="puerto_destino" class="form-control" onchange="calcularCIF(this.value);">
				<option value="">Seleccione una Opción</option>	<?php
			foreach ($resul->result() as $puertos) { 
				?>
			      <option value="<?= $puertos->id; ?>"> <?= $puertos->nombre; ?></option>
			    <?php
			    //print_r($resul);

			}

			?></select><?php
		}else{
			echo "Sin Datos.";
		}

	}

	public function valFOB()
	{
		# code...
		// $salida = $this->input->post('salida');
		$contenedor = $this->input->post('contenedor');
		$puerto_origen = $this->input->post('puerto_origen');
		$peso_tara = $this->input->post('peso_tara');

		try {
			$this->db->join('aduana', 'puerto_nacional.id_gasto_aduana = aduana.id', 'join');
			//$this->db->join('TRM_dolar', '1', 'right');
			$this->db->where('id_puerto', $puerto_origen);
			$this->db->where('id_contenedor', $contenedor);
			$this->db->where('id_peso_tara', $peso_tara);
			$resul = $this->db->get('puerto_nacional');

			// $query = "SELECT *
			// 		FROM puerto_nacional pn
			// 			JOIN aduana a ON pn.id_gasto_aduana = a.id
			// 			LEFT JOIN TRM_dolar tr ON 1 = 1
			// 		WHERE pn.id_puerto = $puerto_orige
			// 		AND pn.id_contenedor = $contenedor";

			// $resul=$this->db->query($query);
			
			
		} catch (Exception $e) {
			echo "Error en la ejecución de la consulta: ".$e;
		}

		if ($resul->num_rows()>0) {
			# code...
			foreach ($resul->result() as $puertos) { 
				$total_FOB_dolar = ($puertos->flete_terrestre_dolar) + ($puertos->gasto_portuario_dolar) + ($puertos->gasto_dolar);
				$total_FOB_pesos = $total_FOB_dolar * 3000; //$puertos->valor_pesos;

				//print_r($puertos);

				echo 
					"Inicio|".
					$puertos->id_puerto."|".
					$puertos->id_contenedor."|".
					$puertos->flete_terrestre_dolar."|".
					$puertos->gasto_portuario_dolar."|".
					$puertos->id_gasto_aduana." AD|".
					$puertos->gasto_dolar."|".
					//$puertos->valor_pesos."|".
					$total_FOB_dolar."|".
					$total_FOB_pesos;
			}
		}else{
			echo "Error al obtener valores FOB.";
		}
	}

	public function valCIF()
	{
		# code...
		$puerto_origen = $this->input->post('puerto_origen');
		$puerto_destino = $this->input->post('puerto_destino');
		$valor_dolar_FOB = $this->input->post('valor_dolar_FOB');
		$valor_pesos_FOB = $this->input->post('valor_pesos_FOB');
		$contenedor = $this->input->post('contenedor');

		//echo "$puerto_origen - $puerto_destino - $valor_dolar_FOB - $valor_pesos_FOB";

		$this->db->where('puerto_id_nal', $puerto_origen);
		$this->db->where('puerto_id_inter', $puerto_destino);
		$this->db->where('contenedor_id', $contenedor);
		// $this->db->join('TRM_dolar', '1', 'left');
		$resul = $this->db->get('puerto_internacional');

		if ($resul->num_rows()>0) {
			# code...
			// print_r($resul);
			foreach ($resul->result() as $puertos) { 
				$total_CIF_dolar = ($puertos->flete_inter_dolar) + ($valor_dolar_FOB);
				$total_CIF_pesos = $total_CIF_dolar * 3000;//$puertos->valor_pesos;

				//print_r($puertos);

				echo 
					"Inicio|".
					$total_CIF_dolar."|".
					$total_CIF_pesos."|".
					$puertos->estimado_dias."|".
					$puertos->frecuencia;
			}
		}else{
			echo "Error al obtener valores CIF.";
		}

	}

	public function nuevaRutaInter()
	{
		# code...
	}

	public function nuevaRutaNal()
	{
		# code...
	}

}

/* End of file main.php */
/* Location: ./application/controllers/main.php */