function limpiar () {
	// body...
	$('#select_peso_tara').empty();
	$('#select_puerto_inter').empty();
	
	if ($('#puerto_origen').val()!="") {
		$('#puerto_origen').val("");
	};
	
}

function llenarPeso () {
	// body...
	var id_contenedor = $("#contenedor").val();
	var id_puerto_origen = $("#puerto_origen").val();

	//alert(id_contenedor);
	//alert(id_puerto_origen);


	$.post('main/llenarPeso', {id_contenedor: id_contenedor, id_puerto_origen:id_puerto_origen }, function(data, textStatus, xhr) {
		/*optional stuff to do after success */

		// alert(data);

		$('#select_peso_tara').empty();
		$('#select_puerto_inter').empty();
		$('#valor_dolar_FOB').val("");
		$('#valor_pesos_FOB').val("");
		$('#valor_dolar_CIF').val("");
		$('#valor_pesos_CIF').val("");
		$("#campo_diasTransito").val("");
		$("#campo_frecuencia").val("");

		$('#select_peso_tara').append(data);

	});

}

function calcularFOB(peso_tara) {
	// body...

	var salida = $("#salida").val();
	var contenedor = $("#contenedor").val();
	var puerto_origen = $("#puerto_origen").val();
	
	// var puerto_origen = $("#puerto_origen").val();

	// alert(puerto_origen+"-"+contenedor+"-"+peso_tara);

	$.post('main/valFOB', {salida: salida, contenedor: contenedor, puerto_origen: puerto_origen, peso_tara:peso_tara }, function(data, textStatus, xhr) {
		/*optional stuff to do after success */

		// alert(data);

		var res_valFOB = data.split("|");

		var valor_dolar_FOB = res_valFOB[7];
		var valor_pesos_FOB = res_valFOB[8];

		$('#valor_dolar_FOB').val(valor_dolar_FOB);
		$('#valor_pesos_FOB').val(valor_pesos_FOB);

		//alert(res_valFOB[1].toString());

		var puerto_id_nal = res_valFOB[1];

		llenarPuertosInter(puerto_id_nal);
 
	});


}

function llenarPuertosInter(puerto_id_nal) {
	// body...
	//alert("LlenarPuertosInter: "+puerto_id_nal);

	var valor_dolar_FOB = $("#valor_dolar_FOB").val();
	var valor_pesos_FOB = $("#valor_pesos_FOB").val();

	$.post('main/llenarPuertosInter', {puerto_id_nal: puerto_id_nal, valor_dolar_FOB: valor_dolar_FOB, valor_pesos_FOB: valor_pesos_FOB }, function(data, textStatus, xhr) {
		/*optional stuff to do after success */

		// alert(data);
		$('#select_puerto_inter').empty();
		$('#valor_dolar_CIF').val("");
		$('#valor_pesos_CIF').val("");
		$("#campo_diasTransito").val("");
		$("#campo_frecuencia").val("");

		$('#select_puerto_inter').append(data);

	});

}

function calcularCIF(puerto_destino) {
	// body...
	// alert("calcularCIF");

	var valor_dolar_FOB = $("#valor_dolar_FOB").val();
	var valor_pesos_FOB = $("#valor_pesos_FOB").val();
	var contenedor = $("#contenedor").val();
	var puerto_origen = $("#puerto_origen").val();
	// var puerto_destino = $( this ).val();

	// alert(valor_dolar_FOB);
	// alert(valor_pesos_FOB);
	// alert(puerto_origen);
	// alert(puerto_destino);

	$.post('main/valCIF', {valor_dolar_FOB: valor_dolar_FOB, valor_pesos_FOB: valor_pesos_FOB, contenedor: contenedor, puerto_origen: puerto_origen, puerto_destino: puerto_destino }, function(data, textStatus, xhr) {
		/*optional stuff to do after success */

		// alert(data);

		var res_valCIF = data.split("|");

		var valor_dolar_CIF = res_valCIF[1];
		var valor_pesos_CIF = res_valCIF[2];
		var valor_diasTransito = res_valCIF[3];
		var valor_frecuencia = res_valCIF[4];

		$("#valor_dolar_CIF").val(valor_dolar_CIF);
		$("#valor_pesos_CIF").val(valor_pesos_CIF);
		$("#campo_diasTransito").val(valor_diasTransito);
		$("#campo_frecuencia").val(valor_frecuencia);

	});


}

